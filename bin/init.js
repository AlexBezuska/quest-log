#!/usr/bin/env node

const fs = require('fs').promises;
const path = require('path');
const copydir = require('copy-dir');
const rootDir = path.join(__dirname, '..');

// Copy directory and log success or errors
function copyDirectory(source, destination) {
  try {
    copydir.sync(source, destination);
    console.log(`Copied directory: ${source} to ${destination}`);
  } catch (err) {
    console.error(`Failed to copy directory from ${source} to ${destination}:`, err);
  }
}

// Copy file and log success or errors
async function copyFile(source, destination) {
  try {
    await fs.copyFile(source, destination);
    console.log(`Copied file: ${source} to ${destination}`);
  } catch (err) {
    console.error(`Failed to copy file from ${source} to ${destination}:`, err);
  }
}

// Initialize the blog structure
async function initBlog() {
  copyDirectory(path.join(rootDir, 'src'), './src');
  copyDirectory(path.join(rootDir, 'dest'), './dest');
  copyDirectory(path.join(rootDir, 'partials'), './partials');
  
  await copyFile(path.join(rootDir, 'quest-log-config.json'), './quest-log-config.json');
  await copyFile(path.join(rootDir, '.init-gitignore'), '.gitignore');
}

// Run the initialization
initBlog();
