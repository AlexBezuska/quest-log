function addNextPrevToFlatPostList(posts) {
    return posts.map((post, i) => {
      if (i > 0) {
        post.next = extractPostMeta(posts[i - 1]);
      }
      if (i < posts.length - 1) {
        post.prev = extractPostMeta(posts[i + 1]);
      }
      return post;
    });
  }
  
  function sortPosts(posts) {
    return posts.sort((a, b) => b.dateTime - a.dateTime);
  }
  
  function onlyFeatured(posts) {
    return posts.filter(post => post.featured === true);
  }
  
  function extractPostMeta(post) {
    return {
      title: post.title,
      date: post.date,
      url: post.url,
      coverPhoto: post.coverPhoto,
      coverPhotAlt: post.coverPhotAlt,
      blurb: post.blurb,
      tags: post.tags,
    };
  }
  
  module.exports = { addNextPrevToFlatPostList, sortPosts, onlyFeatured, extractPostMeta };
  