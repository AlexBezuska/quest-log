#!/usr/bin/env node

const { initBuild, copyStaticAssets } = require('./fileOps');
const { loadConfig } = require('./config');
const fs = require('fs');
const path = require('path');
const handlebars = require('handlebars');
const { marked } = require('marked');

async function main() {
  const config = await loadConfig();
  await initBuild(config);
  copyStaticAssets(config.src, config.dest);
  registerPartials(config.src);
  generateHtmlFromPartials(config.src, config.dest, config);
  generatePosts(config.src, config.dest, config);
  console.log("Build complete.");
}

function registerPartials(src) {
  const partialsDir = path.join(src, 'partials');
  const partialFiles = fs.readdirSync(partialsDir);
  partialFiles.forEach((file) => {
    if (path.extname(file) === '.hbs') {
      const partialName = path.basename(file, '.hbs');
      const partialTemplate = fs.readFileSync(path.join(partialsDir, file), 'utf8');
      handlebars.registerPartial(partialName, partialTemplate);
    }
  });
  
  // Register additional partials if needed
  const additionalPartials = ['header', 'footer']; // add other partial names as required
  additionalPartials.forEach((partial) => {
    const partialPath = path.join(partialsDir, `${partial}.hbs`);
    if (fs.existsSync(partialPath)) {
      const partialTemplate = fs.readFileSync(partialPath, 'utf8');
      handlebars.registerPartial(partial, partialTemplate);
    }
  });
}

function generateHtmlFromPartials(src, dest, config) {
  const pagesDir = path.join(src, 'pages');
  const pageFiles = fs.readdirSync(pagesDir);

  pageFiles.forEach((file) => {
    if (path.extname(file) === '.hbs') {
      const templateName = path.basename(file, '.hbs');
      const templatePath = path.join(pagesDir, file);
      const template = handlebars.compile(fs.readFileSync(templatePath, 'utf8'));
      const jsonDataPath = path.join(pagesDir, `${templateName}.json`);
      const data = fs.existsSync(jsonDataPath) ? JSON.parse(fs.readFileSync(jsonDataPath, 'utf8')) : {};
      const html = template({ ...data, config });
      const outputFileName = path.join(dest, `${templateName}.html`);
      ensureDirectoryExistence(outputFileName);
      fs.writeFileSync(outputFileName, html);
      console.log(`Generated HTML file: ${outputFileName}`);
    }
  });
}

function ensureDirectoryExistence(filePath) {
  const dir = path.dirname(filePath);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
}

function generatePosts(src, dest, config) {
  const postsDir = path.join(src, 'posts');
  
  try {
    const postFolders = fs.readdirSync(postsDir);

    postFolders.forEach((year) => {
      const yearDir = path.join(postsDir, year);

      if (fs.statSync(yearDir).isDirectory()) {
        const monthFolders = fs.readdirSync(yearDir);

        monthFolders.forEach((month) => {
          const monthDir = path.join(yearDir, month);

          if (fs.statSync(monthDir).isDirectory()) {
            const postFiles = fs.readdirSync(monthDir);

            postFiles.forEach((file) => {
              if (fs.statSync(path.join(monthDir, file)).isFile() && path.extname(file) === '.markdown') {
                const markdownContent = fs.readFileSync(path.join(monthDir, file), 'utf8');
                const htmlContent = marked(markdownContent);

                // Define the output file path
                const outputFilePath = path.join(dest, 'posts', year, month, `${path.basename(file, '.markdown')}.html`);
                
                // Ensure the directory exists before writing the file
                ensureDirectoryExistence(outputFilePath);
                
                // Generate the post HTML content using the post partial
                const postTemplate = handlebars.compile('{{> post}}');
                const postHtml = postTemplate({ content: htmlContent, config });

                // Write the HTML content to the file
                fs.writeFileSync(outputFilePath, postHtml);
                console.log(`Generated post HTML file: ${outputFilePath}`);
              }
            });
          }
        });
      }
    });
  } catch (error) {
    console.error('Error during post generation:', error);
  }
}

function ensureDirectoryExistence(filePath) {
  const dir = path.dirname(filePath);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
}
