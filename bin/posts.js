const fs = require('fs');
const path = require('path');
const marked = require('marked');
const yamlFront = require('yaml-front-matter');
const { format } = require('date-fns');
const { createPage } = require('./templates');

function createPostTree(src) {
  const tree = {};
  getDirectories(path.join(src, 'posts')).forEach((year) => {
    tree[year] = {};
    getDirectories(path.join(src, 'posts', year)).forEach((month) => {
      const filesInMonth = fs.readdirSync(path.join(src, 'posts', year, month));
      tree[year][month] = onlyMarkdownFiles(filesInMonth);
    });
  });
  return tree;
}

function returnFlatPostList(tree) {
  const flatPostList = [];
  Object.keys(tree).forEach((year) => {
    Object.keys(tree[year]).forEach((month) => {
      tree[year][month].forEach((post) => {
        const postFile = path.join(src, 'posts', year, month, post);
        const postMeta = getPostMeta(postFile);
        postMeta.date = format(postMeta.dateTime, 'MMMM do, yyyy');
        postMeta.time = format(postMeta.dateTime, 'h:mm a');
        postMeta.url = path.join('/posts', convertFilename(post));
        postMeta.__content = marked(postMeta.__content);
        flatPostList.push(postMeta);
      });
    });
  });
  return flatPostList;
}

function getPostMeta(postFile) {
  const text = fs.readFileSync(postFile, 'utf8');
  return yamlFront.loadFront(text);
}

module.exports = { createPostTree, returnFlatPostList, getPostMeta };
